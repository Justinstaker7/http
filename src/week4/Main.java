/**
 * Part two of the assignment
 */

package week4;

import java.net.*;
import java.io.*;
import java.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

//had help with this one from your video and also a friend that had taken this class in a prev semester
//I noticed in your code in the console there are "keys" that are output after the Js -- I didn't get that with my code

public class Main {

    final String check = ""; //final variable used for requesting web info

    public static void main(String[] args) {

        Main connect = new Main();

        try{
            connect.rebels();
            connect.empire();
        }catch (Exception e){
            System.out.println("error");
        }
    }

    public void rebels() throws Exception {

        URL IMDb = new URL("https://www.imdb.com/title/tt0076759/");

        HttpURLConnection http = (HttpURLConnection) IMDb.openConnection();
        http.setDoOutput(true);
        http.setRequestProperty("check", check);

        int code = http.getResponseCode();

        System.out.println("initiating connection: " + IMDb + "response: " + code);
        System.out.println("--------------------");
    }

    public void empire() throws Exception {

        String IMDb = "https://www.imdb.com/title/tt0076759/";
        URL url = new URL(IMDb);

        try{
            HttpURLConnection xWing = (HttpURLConnection)url.openConnection();
            output(xWing);
        }catch (Exception e){
            System.out.println("error");
        }
    }

    private void output(HttpURLConnection tieFighter) throws Exception{

        if(tieFighter != null){

            try{
                System.out.println("content from IMDb on StarWars: ");
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(tieFighter.getInputStream()));
                String x;

                while ((x = bufferedReader.readLine()) != null){
                    System.out.println(x);
                }
                bufferedReader.close();

            }catch (Exception e){
                System.out.println("error");
            }
        }
    }

}